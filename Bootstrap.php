<?php

namespace robote13\SEOTags;

/**
 * Description of Bootstrap
 *
 * @author Tartharia
 */
class Bootstrap implements \yii\base\BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['robote13/seotags'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => __DIR__ . '/messages',
            'fileMap'=>[
                'robote13/seotags'=>'seotags.php'
            ]
        ];
        if($app instanceof \yii\web\Application)
        {
            $app->set('metatagsManager', 'robote13\SEOTags\components\MetaTagsHandler');
            $app->bootstrap[]='metatagsManager';
        }
    }
}
