<?php

namespace robote13\SEOTags;

/**
 * metatags module definition class
 */
class Module extends \yii\base\Module
{
    public $tagsPanelAcessRule = 'admin';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'robote13\SEOTags\backend\controllers';

    public function behaviors() {
        return[
            'access'=>[
                'class'=> \yii\filters\AccessControl::className(),
                'rules'=>[
                    [
                        'allow'=>true,
                        'roles'=>[$this->tagsPanelAcessRule]
                    ],
                    [
                        'allow'=>false
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setDefaultViewPath();
    }

    private function setDefaultViewPath()
    {
        if(!is_dir($this->viewPath))
        {
            $pos = strrpos($this->controllerNamespace,'\\');
            $this->viewPath = str_replace('\\', '/', ltrim('@'.substr($this->controllerNamespace,0,$pos).'/views','\\'));
        }
    }
}
