SEO manager and behavior for Yii2 models
========================================
This extension provides behavior functions for meta tags and view helper for registering them. 
Automatically loading the correct row from the database using the currently route and params.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist robote13/yii2-seotags-manager "^0.1.0"
```

or add

```
"robote13/yii2-seotags-manager": "^0.1.0"
```

to the require section of your `composer.json` file.
