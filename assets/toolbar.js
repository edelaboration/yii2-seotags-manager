var robote13Toolbar = {
    ajaxSubmit: function(event){
        var form = $("#"+event.target.id), formData = form.serialize(),
        notifyTemplate = '<div data-notify="container" class="col-xs-22 col-sm-16 alert alert-{0}" role="alert">' +
		'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
		'<span data-notify="icon"></span> ' +
		'<span data-notify="title">{1}</span> ' +
		'<span data-notify="message">{2}</span>' +
		'<div class="progress" data-notify="progressbar">' +
			'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
		'</div>' +
		'<a href="{3}" target="{4}" data-notify="url"></a>' +
	'</div>';

        //console.log(event.target);
    $.ajax({
        url: form.attr("action"),
        type: form.attr("method"),
        data: formData,
        success: function (data) {
            form.data('yiiActiveForm').validated = false;
            if(data.message === false)
            {
                for(var key in data.errors) {
                    form.yiiActiveForm('updateAttribute',key,data.errors[key]);
                }
            }else{
                $.notify({
                    icon: 'icon-world',
                    message:data.message
                },{
                    type:'success',
                    element:'#'+form.attr('id'),
                    offset:{
                        y:100
                    },
                    mouseover:'pause',
                    placement:{
                        align:'center',
                        from :'bottom'
                    },
                    template:notifyTemplate
                });
            }
        },
        error: function () {
            $.notify({
                message:'Если вы постоянно видите эту ошибку - обратитесь к администратору сайта',
            },{
                type:'danger',
                mouseover:'pause',
                placement:{
                    align:'center'
                },
                template:notifyTemplate
            });
        }
    });
    }
};

$(document).on('beforeSubmit','#robote13-meta-tags-form',robote13Toolbar.ajaxSubmit).on('submit','#robote13-meta-tags-form', function(e){e.preventDefault();});