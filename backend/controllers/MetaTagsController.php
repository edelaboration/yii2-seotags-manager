<?php

namespace robote13\SEOTags\backend\controllers;

use Yii;
use robote13\SEOTags\models\SeoMeta;
use robote13\SEOTags\models\SeoMetaSearch;
use yii\web\NotFoundHttpException;

/**
 * MetaTagsController implements the CRUD actions for SeoMeta model.
 */
class MetaTagsController extends \robote13\yii2components\web\CrudControllerAbstract
{
    public function getModelClass()
    {
        return SeoMeta::className();
    }

    public function getSearchClass()
    {
        return SeoMetaSearch::className();
    }

    /**
     * Lists all SeoMeta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new SeoMeta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model = new SeoMeta();
        }

        $searchModel = new SeoMetaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single SeoMeta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        throw new NotFoundHttpException();
    }
}
