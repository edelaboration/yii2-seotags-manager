<?php

namespace robote13\SEOTags\backend\controllers;

use robote13\yii2components\web\CrudControllerAbstract;

/**
 * Description of RedirectsController
 *
 * @author Tartharia
 */
class RedirectsController extends CrudControllerAbstract
{
    public function getModelClass()
    {
        return 'robote13\SEOTags\models\Redirect';
    }

    public function getSearchClass() {
        return 'robote13\SEOTags\models\RedirectSearch';
    }
}
