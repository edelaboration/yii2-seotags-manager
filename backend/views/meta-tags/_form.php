<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model robote13\SEOTags\models\SeoMeta */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$("document").ready(function(){
        $("#new-record").on("pjax:end", function() {
            $.pjax.reload({container:"#view-index"});  //Reload GridView
        });
    });'
);
?>

<div class="seo-meta-form">
    <?php Pjax::begin(['id'=>'new-record']); ?>
        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

			<?= $form->field($model, 'route')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'params')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'canonical')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?>
        
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('robote13/seotags', 'Create') : Yii::t('robote13/seotags', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end();?>
    <?php Pjax::end();?>
</div>
