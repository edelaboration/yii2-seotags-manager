<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel robote13\SEOTags\models\SeoMetaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('robote13/seotags', 'Seo Metas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-meta-index">
    <div class="panel panel-default">
        <div class="panel-heading"><?=Yii::t('robote13/seotags', 'Create Seo Meta')?></div>
        <div class="panel-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(['id'=>'view-index']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
           'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                  'id',
                  'route',
                  'params',
                  'canonical',
                  'title',
                  // 'description',
                  // 'keywords',
                  // 'tags:ntext',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=> "{update} {delete}"
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>
