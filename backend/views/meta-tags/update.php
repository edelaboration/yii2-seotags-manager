<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model robote13\SEOTags\models\SeoMeta */

$this->title = Yii::t('robote13/seotags', 'Update {modelClass}: ', [
    'modelClass' => 'Seo Meta',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('robote13/seotags', 'Seo Metas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('robote13/seotags', 'Update');
?>
<div class="seo-meta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
