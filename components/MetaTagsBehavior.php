<?php

namespace robote13\SEOTags\components;

use Yii;
use yii\db\ActiveRecord;
use robote13\SEOTags\models\SeoMeta;

/**
 * Description of SeoTagsBehavior
 *
 */
class MetaTagsBehavior extends \yii\base\Behavior{

    public $viewRoute;

    private $_params;

    private $_seo;

    public function attach($owner) {
        parent::attach($owner);
        if(!$this->owner instanceof ActiveRecord)
        {
            throw new \yii\base\InvalidParamException('Not instance of ');
        }
    }

    public function events() {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete'
        ];
    }

    /**
     *
     * @return \robote13\SEOTags\models\SeoMeta
     */
    public function getSeo()
    {
        if(!isset($this->_seo))
        {
            $this->_seo = $this->getSeoTag()->one();
            if(empty($this->_seo))
            {
                $this->_seo = Yii::createObject([
                    'class' => SeoMeta::className(),
                    'route' => $this->viewRoute
                ]);
            }
        }
        return $this->_seo;
    }

    public function afterValidate($event)
    {
        if(!$this->seo->load(\Yii::$app->getRequest()->post()) && !$this->seo->validate())
        {
            $this->owner->addError('id','Relational invalid');
        }
    }

    /**
     *
     * @param \yii\base\Event $event
     */
    public function afterSave($event)
    {
        $this->seo->params = $this->viewParams;
        $this->seo->save(false);
    }

    /**
     *
     * @param \yii\base\Event $event
     */
    public function afterDelete($event)
    {
        $this->seo->delete();
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    protected function getSeoTag()
    {
        $query = SeoMeta::find()->forRow($this->viewRoute, $this->viewParams);
        $query->multiple = false;
        return $query;
    }

    /**
     *
     * @param [] $params массив вида paramName=>keyAttributeName где ключ - имя гет параметра,
     * а значение - имя атрибута значение которого является ключом для поиска модели
     */
    public function setViewParams($params)
    {
        $this->_params = $params;
    }

    protected function getViewParams()
    {
        return serialize(array_combine(array_keys($this->_params), array_map(function($param) {
            return (string)$this->owner->{$param};
        }, $this->_params)));
    }
}