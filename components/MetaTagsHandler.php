<?php

namespace robote13\SEOTags\components;

use Yii;
use yii\web\View;
use yii\helpers\Url;
use yii\web\Application;
use yii\helpers\ArrayHelper;
use robote13\SEOTags\models\SeoMeta;

/**
 * Description of MetaTagsHandler
 *
 * @property-read \robote13\SEOTags\models\SeoMeta $model Description
 * @author Tartharia
 */
class MetaTagsHandler extends \yii\base\Component
{

    /**
     * @var string name of the parameter storing the current page index.
     */
    public $pageParam = 'page';

    /**
     * Meta tag  "noindex,follow" will be added if true
     * @var boolean
     * @since 0.1.4
     */
    public $noindexPagination = false;

    /**
     * @var \robote13\SEOTags\models\SeoMeta
     */
    protected $_model = null;
    private $_curentPage;

    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->view->on(View::EVENT_BEGIN_PAGE, [$this, 'applyMeta']);
        Yii::$app->on(Application::EVENT_BEFORE_REQUEST, function () {
            Yii::$app->getView()->on(View::EVENT_END_BODY, [$this, 'renderToolbar']);
        });
    }

    /**
     *
     * @return \robote13\SEOTags\models\SeoMeta
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     *
     * @param \yii\base\ViewEvent $event
     */
    protected function applyMeta($event)
    {
        /* @var $view \yii\web\View */
        $view = $event->sender;
        $this->_curentPage = Yii::$app->getRequest()->get($this->pageParam);
        if($this->loadMetaData())
        {
            $view->title = $this->paginateTitle($this->_model->title);
            $this->registerMetaTags($this->_model->prepareTags(), $view);
        }
        $this->addNoindex($view);
    }

    protected function renderToolbar($event)
    {
        /* @var $view \yii\web\View */
        $view = $event->sender;
        if(Yii::$app->user->can(ArrayHelper::getValue(Yii::$app->getModule('seo'), 'tagsPanelAcessRule', false)) && !Yii::$app->request->isAjax)
        {
            echo Yii::$app->runAction('/seo/panel/index', [
                'route' => Yii::$app->requestedRoute,
                'params' => !empty(Yii::$app->controller->actionParams) ? serialize(Yii::$app->controller->actionParams) : null
            ]);
            \robote13\SEOTags\ToolbarAsset::register($view);
        }
    }

    /**
     *
     * @return boolean
     */
    private function loadMetaData()
    {
        $params = Yii::$app->getRequest()->get() ? serialize(Yii::$app->controller->actionParams) : '';
        $this->_model = SeoMeta::find()->forRow(Yii::$app->requestedRoute, $params)->one();
        return $this->_model !== null;
    }

    /**
     *
     * @param array $metatags
     * @param View $view
     */
    private function registerMetaTags(array $metatags, View $view)
    {
        if($this->_model->canonical)
        {
            $view->registerLinkTag(['rel' => 'canonical', 'href' => Url::to($this->_model->canonical, true)], 'meta-canonical');
        } elseif($this->_curentPage == 1)
        {
            $view->registerLinkTag(['rel' => 'canonical', 'href' => Url::current($this->clearParams(), true)], 'meta-canonical');
        }
        foreach ($metatags as $key => $value)
        {
            if(!empty($value))
                $view->registerMetaTag($value, $key);
        }
    }

    private function addNoindex(View $view)
    {
        if($this->noindexPagination && (int) $this->_curentPage > 1)
        {
            if(!ArrayHelper::keyExists('meta-canonical', $view->linkTags))
            {
                $view->registerLinkTag(['rel' => 'canonical', 'href' => Url::current($this->clearParams(), true)], 'meta-canonical');
            }
            $view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,follow']);
        }
    }

    private function paginateTitle($title)
    {
        return $this->_curentPage > 1 ? Yii::t('robote13/seotags', "{title}. Page {page}", ['title' => $title, 'page' => $this->_curentPage]) : $title;
    }

    private function clearParams()
    {
        return array_map(function($item) {
            return in_array($item, Yii::$app->requestedParams) ? $item : null;
        }, Yii::$app->request->get());
    }

}
