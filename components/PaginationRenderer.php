<?php

namespace robote13\SEOTags\components;

use Yii;

/**
 * Cutting out blocks with texts on pagination.
 *
 * @author Tartharia
 */
class PaginationRenderer extends \yii\base\ViewRenderer{

    /**
     * @var string the marker of the beginning of the block that hidden on pagination
     */
    public $startBlock='<!--p-hidden-->';

    /**
     * @var string the marker of the end of the block that hidden on pagination
     */
    public $endBlock='<!--/p-hidden-->';

    /**
     * @var string name of the parameter storing the current page index.
     */
    public $pageParam = 'page';

    /**
     * Renders a view file.
     * @param \yii\web\View $view
     * @param string $file the view file. This can be either an absolute file path or an alias of it.
     * @param type $params the parameters (name-value pairs) that will be extracted and made available in the view file.
     */
    public function render($view, $file, $params) {
        $output = $view->renderPhpFile($file, $params);
        $page = Yii::$app->request->get($this->pageParam,1);
        if((int)$page === 1)
        {
            return $output;
        }
        while (($beginBlock = mb_strpos($output, $this->startBlock)) !== false) {
            $endBlock = mb_strpos($output, $this->endBlock, $beginBlock);
            if(!$endBlock){
                throw new \RuntimeException(Yii::t("robote13/seotags","The start of the block for hidden on pagination is found, but there is no block closing marker. {file}",['file'=> Yii::getAlias($file)]));
            }
            $output = mb_substr($output,0, $beginBlock) . mb_substr($output, $endBlock + strlen($this->endBlock));
        }

        return $output;
    }
}