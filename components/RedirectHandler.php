<?php

namespace robote13\SEOTags\components;

use Yii;
use robote13\SEOTags\models\Redirect;

/**
 * Description of RedirectHandler
 *
 * @author Tartharia
 */
class RedirectHandler extends \yii\web\ErrorHandler
{

    public function handleException($exception)
    {
        if($exception instanceof \yii\web\NotFoundHttpException)
        {
            $redirect = Redirect::find()
                    ->where(['old_url_hash' => md5(Yii::$app->request->url)])
                    ->asArray()
                    ->one();

            if($redirect)
            {
                Yii::$app->response->redirect($redirect['new_url'], $redirect['status'], false)->send();
                return;
            }
            if($this->tryWithoutParams())
            {
                return;
            }
        }
        return parent::handleException($exception);
    }

    private function tryWithoutParams()
    {
        $redirect = Redirect::find()
                ->where(['old_url_hash' => md5('/' . Yii::$app->request->pathInfo)])
                ->asArray()
                ->one();
        if($redirect)
        {
            Yii::$app->response->redirect($redirect['new_url'] . '?' . Yii::$app->request->queryString, $redirect['status'], false)->send();
            return true;
        }
        return false;
    }

}
