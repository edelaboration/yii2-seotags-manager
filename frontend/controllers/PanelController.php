<?php

/*
 * The MIT License
 *
 * Copyright 2017 Tartharia.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace robote13\SEOTags\frontend\controllers;

use Yii;
use yii\web\Response;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use robote13\SEOTags\models\SeoMeta;

/**
 * Description of PanelController
 *
 * @author Tartharia
 */
class PanelController extends \yii\web\Controller{

    public function actionIndex($route,$params=null)
    {
        $model = $this->getModel($route, $params);
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ['errors'=>ActiveForm::validate($model),'message' => false];
            if(count($result['errors'])===0 && $model->save(false))
            {
                $result['message'] = Html::tag('p', Yii::t('robote13/seotags','Meta tags was updated.'));
            }
            return $result;
        }
        return $this->renderPartial('index', ['model'=>$model,'route'=>$route,'params'=>$params]);
    }

    /*public function actionUpdateTags()
    {

    }*/

    /**
     *
     * @param string $route
     * @param string|null $params
     * @return SeoMeta
     */
    protected function getModel($route,$params)
    {
        $model = SeoMeta::find()
                ->where(['route'=> $route])
                ->andWhere(['or','params=:params','params IS NULL'],['params'=>$params])
                ->one();
        if(!$model)
        {
            $model = Yii::createObject([
                'class'=>SeoMeta::className(),
                'route' => $route,
                'params'=> $params
            ]);
        }
        return $model;
    }
}
