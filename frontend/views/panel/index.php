<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model robote13\SEOTags\models\SeoMeta */
?>

<div class="panel-tags container-fluid hidden-print">
    <div class="panel">
    <div class="panel-body">

    <?php  $form = ActiveForm::begin([
        'id'=>'robote13-meta-tags-form',
        'action'=>['/seo/panel/index','route'=>$model->route,'params'=>$model->params],
        'options'=>['style'=>'position:relative'],
        'enableClientValidation'=>true,
        'enableAjaxValidation'=>false,
        'fieldConfig'=>[
            'inputOptions'=>['class'=>'form-control input-sm']
        ]
    ])?>

        <?=$form->field($model,'title');?>
        <?=$form->field($model,'keywords');?>
        <?=$form->field($model,'description')->textarea(['rows'=>2]);?>
        <?=$form->field($model,'canonical');?>
     <?=Html::submitButton('Update',['class'=>'btn btn-primary btn-sm'])?>
    <?php    ActiveForm::end()?>
    </div>
    </div>
</div>