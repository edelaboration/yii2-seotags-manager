<?php

use yii\db\Migration;

/**
 * Handles the creation for table `metatags_table`.
 */
class m160504_091352_create_metatags_table extends Migration
{
    public $table = "{{%seo_meta}}";

    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'route' => $this->string()->notNull(),
            'params' => $this->string()->null()->defaultValue(null),
            'canonical' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'keywords' => $this->string(),
            'tags'=> $this->text()->defaultValue(null)
        ],'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci');
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}