<?php

use yii\db\Migration;

/**
 * Handles the creation of table `redirect`.
 */
class m170123_103643_create_redirect_table extends Migration
{
    public $table = "{{%redirect}}";
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'old_url' => $this->string()->notNull(),
            'old_url_hash' => $this->char(32),
            'new_url' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(301)
        ],'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci');

        $this->createIndex('old_url_idx', $this->table, 'old_url_hash', true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
