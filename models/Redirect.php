<?php

namespace robote13\SEOTags\models;

use Yii;
use robote13\yii2components\behaviors\IndexedStringBehavior;

/**
 * This is the model class for table "{{%redirect}}".
 *
 * @property integer $id
 * @property string $old_url
 * @property string $old_url_hash
 * @property string $new_url
 * @property integer $status
 */
class Redirect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%redirect}}';
    }

    public function behaviors()
    {
        return[
            'indexed'=>[
                'class'=> IndexedStringBehavior::className(),
                'attribute' => 'old_url',
                'indexAttribute' => 'old_url_hash'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_url', 'new_url'], 'required'],
            [['status'], 'integer'],
            [['old_url', 'new_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('robote13/seotags', 'ID'),
            'old_url' => Yii::t('robote13/seotags', 'Old Url'),
            //'old_url_hash' => Yii::t('robote13/seotags', 'Old Url Hash'),
            'new_url' => Yii::t('robote13/seotags', 'New Url'),
            'status' => Yii::t('robote13/seotags', 'Status'),
        ];
    }
}
