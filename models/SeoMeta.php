<?php

namespace robote13\SEOTags\models;

use Yii;

/**
 * This is the model class for table "{{%seo_meta}}".
 *
 * @property integer $id
 * @property string $route
 * @property string $params
 * @property string $canonical
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $tags
 */
class SeoMeta extends \yii\db\ActiveRecord
{
    const SCENARIO_REGISTER = 'register_meta';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seo_meta}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['route'], 'required'],
            [['tags'], 'string'],
            [['canonical', 'title', 'description', 'keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('robote13/seotags', 'ID'),
            'route' => Yii::t('robote13/seotags', 'Route'),
            'params' => Yii::t('robote13/seotags', 'Params'),
            'canonical' => Yii::t('robote13/seotags', 'Canonical'),
            'title' => Yii::t('robote13/seotags', 'Title'),
            'description' => Yii::t('robote13/seotags', 'Description'),
            'keywords' => Yii::t('robote13/seotags', 'Keywords'),
            'tags' => Yii::t('robote13/seotags', 'OG Tags'),
        ];
    }

    /**
     *
     */
    public function prepareTags()
    {
        $tagNames = ['description','keywords'];
        $metatags = [];
        foreach ($tagNames as $metatag)
        {
            if(($value = $this->getAttribute($metatag)))
            {
                $metatags[$metatag]=['name'=>$metatag,'content'=>$value];
            }
        }
        return $metatags;
    }

    /**
     * @inheritdoc
     * @return SeoMetaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SeoMetaQuery(get_called_class());
    }
}
