<?php

namespace robote13\SEOTags\models;

/**
 * This is the ActiveQuery class for [[SeoMeta]].
 *
 * @see SeoMeta
 */
class SeoMetaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function forRow($route,$params)
    {
        return $this->where(['route'=> $route])->andWhere(['or','params=:params','params IS NULL'],['params'=>$params]);
    }

    /**
     * @inheritdoc
     * @return SeoMeta[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SeoMeta|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
